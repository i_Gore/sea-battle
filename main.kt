package com

import com.seafight.igore.*

fun main() {


    var my_board1 = Board(10)
    var enemy_board1 = Board(10)
    var my_board2 = Board(10)
    var enemy_board2 = Board(10)
    println("Player 1, please enter your ships")
    var shipCounter = 0
    while(true) {
        var console = readLine()
        val(size,c1,c2,dir) = console!!.split(" ")
        if(my_board1.addShip(size.toInt(), Cell(c1.toInt(), c2.toInt()),dir)) {
            println("Your ship has successfully placed")
            shipCounter++
            println("Current number of ships: $shipCounter")
        }
        else {
            println("You can't put the ship on this place")
        }
        if(shipCounter == 10) {
            break
        }
    }
    println("Your ships have been succesfully placed, please give device to another player")
    println();
    for(i in 9 downTo 0) {
        for(j in 1..10) {
            if(my_board1.cells[10*i+j].status == CELL_STATUS_ALIVE)
                print("= ");
            else if(my_board1.cells[10*i+j].status == CELL_STATUS_EMPTY)
                print(". ");
        }
        println();
    }
    for(i in 1..20) println()
    println("Player 2, please enter your ships");
    shipCounter = 0
    while(true) {
        var console = readLine()
        val(size,c1,c2,dir) = console!!.split(" ")
        if(my_board2.addShip(size.toInt(), Cell(c1.toInt(), c2.toInt()),dir)) {
            println("Your ship has successfully placed")
            shipCounter++
            println("Current number of ships: $shipCounter")
        }
        else {
            println("You can't put the ship on this place")
        }
        if(shipCounter == 10) {
            break
        }
    }
    println("Your ships have been successfully placed, please give device to another player")
    for(i in 9 downTo 0) {
        for(j in 1..10) {
            if(my_board2.cells[10*i+j].status == CELL_STATUS_ALIVE)
                print("= ");
            else if(my_board2.cells[10*i+j].status == CELL_STATUS_EMPTY)
                print(". ");
        }
        println();
    }
    for(i in 1..20) println()




    var turn = 1;
    println("The game has started, good luck")
    while(true) {
        if (turn == 1) {
            println("Player1's turn")
            for (i in 9 downTo 0) {
                for (j in 1..10) {
                    when {
                        enemy_board1.cells[10 * i + j].status == CELL_STATUS_EMPTY -> print(". ")
                        enemy_board1.cells[10 * i + j].status == CELL_STATUS_FIRED -> print("# ")
                        enemy_board1.cells[10 * i + j].status == CELL_STATUS_DEAD -> print("X ")
                        enemy_board1.cells[10 * i + j].status == CELL_STATUS_MISSED -> print("* ")
                    }
                }
                println();
            }
            var console = readLine()
            val (x1, y1) = console!!.split(" ")
            var tempcell = my_board2.fire(Cell(x1.toInt(), y1.toInt())) // тут подстрелится доска игрока 2 своя
            when {
                tempcell.status == CELL_STATUS_MISSED -> for(i in enemy_board1.cells) {
                    if (i.x == tempcell.x && i.y == tempcell.y) {
                        i.status = tempcell.status
                        turn = 2
                        break
                    }
                }
                tempcell.status == CELL_STATUS_FIRED -> for (i in enemy_board1.cells) {
                    if (i.x == tempcell.x && i.y == tempcell.y) { // здесь на той же клетке поменяем статус для доски
                        i.status = tempcell.status // противника первого игрока
                        break
                    }
                }
                tempcell.status == CELL_STATUS_DEAD -> for(i in my_board2.cells) {
                    if(i.status == CELL_STATUS_DEAD) {
                        for(j in enemy_board1.cells) {
                            if(i.x == j.x && i.y == j.y) j.status = i.status
                        }
                    }
                }
            }
            println(tempcell.status)
            var deadcount = 0;
            for(i in my_board2.cells) {
                if(i.status == CELL_STATUS_DEAD) deadcount++;
            }
            if(deadcount == 20) {
                println("Congratulations! Player 1 has won the game!!!!")
                for (i in 9 downTo 0) {
                    for (j in 1..10) {
                        when {
                            enemy_board1.cells[10 * i + j].status == CELL_STATUS_EMPTY -> print(". ")
                            enemy_board1.cells[10 * i + j].status == CELL_STATUS_FIRED -> print("# ")
                            enemy_board1.cells[10 * i + j].status == CELL_STATUS_DEAD -> print("X ")
                            enemy_board1.cells[10 * i + j].status == CELL_STATUS_MISSED -> print("* ")
                        }
                    }
                    println();
                }
                break
            }
            deadcount = 0;
        }
        else if (turn == 2) {
            println("Player2's turn")
            for (i in 9 downTo 0) {
                for (j in 1..10) {
                    when {
                        enemy_board2.cells[10 * i + j].status == CELL_STATUS_EMPTY -> print(". ")
                        enemy_board2.cells[10 * i + j].status == CELL_STATUS_FIRED -> print("# ")
                        enemy_board2.cells[10 * i + j].status == CELL_STATUS_DEAD -> print("X ")
                        enemy_board2.cells[10 * i + j].status == CELL_STATUS_MISSED -> print("* ")
                    }
                }
                println();
            }
            var console = readLine()
            val (x2, y2) = console!!.split(" ")
            var tempcell = my_board1.fire(Cell(x2.toInt(), y2.toInt())) // тут подстрелится доска игрока 1 своя
            when {
                tempcell.status == CELL_STATUS_MISSED -> for(i in enemy_board2.cells) {
                    if (i.x == tempcell.x && i.y == tempcell.y) {
                        i.status = tempcell.status
                        turn = 1
                        break
                    }
                }
                tempcell.status == CELL_STATUS_FIRED -> for (i in enemy_board2.cells) {
                    if (i.x == tempcell.x && i.y == tempcell.y) {
                        i.status = tempcell.status
                        break
                    }
                }
                tempcell.status == CELL_STATUS_DEAD -> for(i in my_board1.cells) {
                    if(i.status == CELL_STATUS_DEAD) {
                        for(j in enemy_board2.cells) {
                            if(i.x == j.x && i.y == j.y) j.status = i.status
                        }
                    }
                }
            }
            println(tempcell.status)
            var deadcount = 0;
            for(i in my_board1.cells) {
                if(i.status == CELL_STATUS_DEAD) deadcount++;
            }
            if(deadcount == 20) {
                println("Congratulations! Player 2 has won the game!!!!")
                for (i in 9 downTo 0) {
                    for (j in 1..10) {
                        when {
                            enemy_board2.cells[10 * i + j].status == CELL_STATUS_EMPTY -> print(". ")
                            enemy_board2.cells[10 * i + j].status == CELL_STATUS_FIRED -> print("# ")
                            enemy_board2.cells[10 * i + j].status == CELL_STATUS_DEAD -> print("X ")
                            enemy_board2.cells[10 * i + j].status == CELL_STATUS_MISSED -> print("* ")
                        }
                    }
                    println();
                }
                break
            }
            deadcount = 0;
        }
    }
}