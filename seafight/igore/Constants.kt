package com.seafight.igore

const val  CELL_STATUS_EMPTY = "EMPTY"
const val  CELL_STATUS_MISSED = "MISSED"
const val  CELL_STATUS_FIRED = "FIRED"
const val  CELL_STATUS_ALIVE = "ALIVE"
const val  CELL_STATUS_DEAD = "DEAD"

const val  SHIP_DIRECTION_NORTH = "N"
const val  SHIP_DIRECTION_EAST = "E"
const val  SHIP_DIRECTION_WEST = "W"
const val  SHIP_DIRECTION_SOUTH = "S"

const val SHIP_STATUS_ALIVE = "ALIVE"
const val SHIP_STATUS_FIRED = "FIRED"
const val SHIP_STATUS_DEAD = "DEAD"