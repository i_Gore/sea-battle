package com.seafight.igore

import io.vertx.core.AbstractVerticle
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.StaticHandler
class Serverhtml: AbstractVerticle() {


    override fun start() {
        val boardController = BoardController(vertx);
        var router = Router.router(vertx)


        vertx.createHttpServer()
            .requestHandler(router)
            .listen(80)
    }
}