package com.seafight.igore

import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.templ.freemarker.FreeMarkerTemplateEngine

class BoardController(vertx: Vertx) {
    val engine = FreeMarkerTemplateEngine.create(Vertx.vertx())
    fun indexHandle(ctx: RoutingContext) {
        var data = JsonObject()
        var tableArray = JsonArray()
        for(k in 1..10) {
            var tableInner = JsonArray()
            for(h in 1..10) {
                tableInner.add(h)
            }
            tableArray.add(k)
        }
        data.put("tableArray",tableArray)
        engine.render(data,"board.ftl") {
            if(it.succeeded()) {
                ctx.response().end(it.result());
            } else {
                ctx.response().end(it.cause().message)
            }
        }
        ctx.response().end("Privet Andrey")
    }
    fun privetHandler(ctx: RoutingContext) {
        ctx.response().end("Privet Andrey, kak dela?")
    }
}