package com.seafight.igore

class Board {
    private val sizeBoard = 10
    var cells = arrayListOf<Cell>()
    var ships = arrayListOf<Ship>()

    constructor(sizeBoard: Int) {
        init(this.sizeBoard)
    }

    fun check(ourShip: Ship): Boolean { // проверяет чтоб не ставили корабли на и около существующего
        ships.forEach { ship ->
            ship.cells.forEach { shipCell ->
                (-1..1).forEach { i ->
                    (-1..1).forEach { j ->
                        ourShip.cells.forEach { ourCell ->
                            if (ourCell.x + i == shipCell.x && ourCell.y + j == shipCell.y) return false
                        }
                    }
                }
            }
        }
        return true
    }

    private fun init(sizeBoard: Int) { // инциализирует и добавляет массив клеток доски
        cells.add(Cell(0,0));
        for (y in 1..sizeBoard) for (x in 1..sizeBoard) cells.add(Cell(x, y))
    }

    fun addShip(size: Int, cell: Cell, dir: String): Boolean { // добавился ли корабль или нет (проверяет чеком)
        var newShip = Ship(size)
        return if (newShip.make(cell, dir)) {
            if (this.check(newShip)) {
                ships.add(newShip)
                for(i in this.cells) {
                    for(j in newShip.cells) {
                        if(i.x == j.x && i.y == j.y) {//i это у нас клетки доски j КЛЕТКИ а не счетчик кораблей
                            i.status = j.status;
                        }
                    }
                }
                true
            } else false
        } else false
    }

    fun fire(cell: Cell): Cell { // стреляем, причем эт функция доски
        cell.status = CELL_STATUS_MISSED //изначально типа промазали
        for (ship in ships) { //идем по всем клеткам кораблей, причем если клетка не принадлежит кораблю, то ничего она не будет делать
            val statusOfFire = ship.fire(cell) // то есть запускаем функцию для корабля
            if (statusOfFire == SHIP_STATUS_FIRED) {
                cell.status = statusOfFire
                for(i in this.cells) {
                    if(i.x == cell.x && i.y == cell.y) i.status = cell.status
                }
            }
            else if(statusOfFire == SHIP_STATUS_DEAD) {
                for(i in ship.cells) {
                    i.status = CELL_STATUS_DEAD
                    for(j in this.cells) {
                        if(j.x == i.x && j.y == i.y) {
                            j.status = i.status;
                        }
                    }
                }
                cell.status = statusOfFire;
            }
        }
        if(cell.status == CELL_STATUS_MISSED) {
            for(i in this.cells) {
                if(i.x == cell.x && i.y == cell.y) i.status = cell.status
            }
        }
        return cell
    }
}