package com.seafight.igore

class Ship(var size: Int) {
    var status: String = "Alive"
    var deadCells = 0
    var cells = arrayListOf<Cell>() // это клетки корабля в них кидаем клетки доски

    fun make(cell: Cell, dir: String): Boolean {
        when (dir) { // в зависимости от направления
            SHIP_DIRECTION_NORTH -> for (i in 0 until size) {
                if (cell.y + i > 10) {
                    return false
                }
                cells.add(Cell(cell.x, cell.y + i)) // на севере по игрику вверх
            }
            SHIP_DIRECTION_SOUTH -> for (i in 0 until size) {
                if (cell.y - i < 1) {
                    return false
                }
                cells.add(Cell(cell.x, cell.y - i)) // юг игрик вниз
            }
            SHIP_DIRECTION_WEST -> for (i in 0 until size) {
                if (cell.x - i < 1) {
                    return false
                }
                cells.add(Cell(cell.x - i, cell.y)) // запад налево
            }
            SHIP_DIRECTION_EAST -> for (i in 0 until size) {
                if (cell.x + i > 10) {
                    return false
                }
                cells.add(Cell(cell.x + i, cell.y)) // восток направо
            }
            else -> {
                println("Найди свой жизненный путь, парниша")
                return false
            }
        }
        for(i in this.cells) {
            i.status = CELL_STATUS_ALIVE
        }
        return true
    }

    fun fire(cell: Cell): String { //это именно корабля функция, сдох или не сдох
        var cellReturnStatus = SHIP_STATUS_ALIVE // сперва жив
        for (ourCell in this.cells) {
            if (cell.x == ourCell.x && cell.y == ourCell.y) { // попали в клетку корабля
                ourCell.status = CELL_STATUS_FIRED // то есть это в доске хранится
                this.status = SHIP_STATUS_FIRED // это говорит, что именно в корабль попали
                cellReturnStatus = SHIP_STATUS_FIRED // корабль поражен
                this.deadCells++
            }
        }
        if (this.deadCells == this.size) { // если все клетки померли, то корабль помер
            this.status = SHIP_STATUS_DEAD
            if (cellReturnStatus == SHIP_STATUS_FIRED) {
                cellReturnStatus = SHIP_STATUS_DEAD //возвращаем, что корабль умер
            }
        }

        return cellReturnStatus
    }
}